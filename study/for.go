package main

func main() {

	// 1、最基础的方式，单个循环条件。
	//i := 1
	//for i <= 3 {
	//	fmt.Println(i)
	//	i += 1
	//}

	// 2、经典的初始/条件/后续 `for` 循环。
	//for i := 1; i <= 3; i++ {
	//	fmt.Println(i)
	//}

}
