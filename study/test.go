package main

import (
	"crypto/sha1"
	"encoding/hex"
	"fmt"
)

func SHA1(s string) string {

	o := sha1.New()

	o.Write([]byte(s))

	return hex.EncodeToString(o.Sum(nil))

}

func main() {
	fmt.Println(SHA1("00\n<ExpressRes>\n\t<stuNo>12001234</stuNo>\n\t<icno>65</icno>\n\t<stuName>吴超</stuName>\n\t<certNo>12001234</certNo>\n\t<bal>3.10</bal>\n\t<status>01</status>\n\t<remark/>\n\t<cards>\n\t\t<card>\n\t\t\t<icno>65</icno>\n\t\t\t<bal>3.10</bal>\n\t\t\t<inAmt>0</inAmt>\n\t\t\t<status>01</status>\n\t\t\t<remark>有效卡</remark>\n\t\t</card>\n\t</cards>\n</ExpressRes>L32R8GO2KJ7MIELL"))
}
