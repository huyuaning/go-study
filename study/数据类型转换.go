package main

import "fmt"

func main() {

	//Golang和java/c不同，Go在不同类型的变量之间赋值时需要显式转换。也就是说Golang中数据类型不能自动转换。
	//表达式 T(v) 将值v转换为类型T
	//T：就是数据类型，比如 int32，int64，float32等等
	//v：就是需要转换的变量

	var a int32 = 100 //定义变量

	var b float32 = float32(a) //转换成浮点数

	fmt.Printf("%T,%v", b, b)

}
