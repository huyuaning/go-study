package main

import "fmt"

func main() {

	//& 为 “取址” 运算符
	//* 为 “取值” 运算符

	//指针是一个变量，它的值是另一个变量的内存地址
	//必须先声明一个指针变量，才能使用它来存储内存地址 var name *type

	var num int = 30
	fmt.Println(&num) //这里输出内存地址 0xc000016058

	var ptr *int = &num //定义 ptr 为指针变量
	fmt.Println(ptr)    //这里输出内存地址 0xc000016058

	//这里输出的是指针变量自己的内存地址
	fmt.Println(&ptr)

	//用 *name 可以取出内存地址中的值
	fmt.Println(*ptr) //输出 值本身

	//重新赋值为
	*ptr = 300
	fmt.Println(num) //上面被重新赋值了，这里就会输出 300

}
