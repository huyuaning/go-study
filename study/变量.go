package main

import "fmt"

func main() {

	var a = "初始化"
	fmt.Println(a)

	//一次声明多个变量
	var b, c string = "测试", "测试2"
	fmt.Println(b)
	fmt.Println(c)

	var d int //没有赋值，为默认值
	fmt.Println(d)

	// `:=` 语法是声明并初始化变量的简写，例如
	// 这个例子中的 `var f string = "飞翔"`。
	f := "飞翔"
	fmt.Println(f)

}
