package main

GOPATH 是一个环境变量，用来表明你写的go项目的存放路径， 然后再建立三个子文件夹(子文件夹名必须为src、pkg、bin)

命令行 go env 查看 GOPATH

所有的项目代码都放到GOPATH的src目录下

- src目录：在工程经过go build、go install或go get等指令后，会将下载的第三方包源代码文件放在
- bin目录：产生的二进制可执行文件
- pkg目录：生成的中间缓存/编译后的文件

如果我们使用版本管理工具来管理我们的项目代码时，我们只需要添加$GOPATH/src目录的源代码即可。bin 和 pkg 目录的内容无需版本控制

适合个人开发者项目结构，们知道源代码都是存放在GOPATH的src目录下，那我们可以按照下图来组织我们的代码。

![](http://www.topgoer.com/static/2/5.png)

目前流行的项目结构

![](http://sdongpo-h2fish.oss-cn-hangzhou.aliyuncs.com/uPic/KzfMFZ.png)
